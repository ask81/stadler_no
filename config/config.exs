# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint

signing_salt = :crypto.strong_rand_bytes(32) |> Base.encode64 |> binary_part(0, 32)

config :stadler_no, StadlerNoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ctOsVxSz13O/4dIiop7CL96uuTNHJlU5iN31dDcBdSwxmtwj3ZMJ2UDtlkNRBXOr",
  render_errors: [view: StadlerNoWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: StadlerNo.PubSub,
  code_reloader: false,
  live_view: [signing_salt: signing_salt]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.

config :os_mon,
  start_disksup: false
  
import_config "#{Mix.env()}.exs"

