import Config

IO.inspect "release config is started"
application_port = System.fetch_env!("PORT")
admin_pwd = System.fetch_env!("ADMIN_PWD")
IO.inspect admin_pwd
#secret_key_base = System.fetch_env!("Stadler_NO_SKB")
#live_view_signing_salt= System.fetch_env!("Stadler_NO_LV_SS")
secret_key_base = :crypto.strong_rand_bytes(64) |> Base.encode64 |> binary_part(0, 64)
live_view_signing_salt = :crypto.strong_rand_bytes(64) |> Base.encode64 |> binary_part(0, 64)
#live_view_signing_salt= System.fetch_env!("Stadler_NO_LV_SS")
    #transport_options: [socket_opts: [:inet6]]
  #],

config :stadler_no, :password, admin_pwd

config :stadler_no, StadlerNoWeb.Endpoint,
  http: [:inet6, port: String.to_integer(application_port)],
  secret_key_base: secret_key_base,
  live_view: [signing_salt: live_view_signing_salt]


config :stadler_no, StadlerNo.Endpoint, server: true



