defmodule StadlerNoWeb.LedThermometer do
  use StadlerNoWeb, :live_component

  def title(), do: "Ginormous LED thermometer for Africa Burn"
  def route(), do: "led"

  def render(assigns) do
    ~L"""
<h1> <%= title() %> </h1>
<hr class="solid">
    <img width="100%" src="https://raw.githubusercontent.com/askasp/AB-LedThermostat/master/saunandtermo.png"/>
<a href="https://github.com/askasp/AB-LedThermostat">  Source code </a>

    <p>
    For Africa Burn 2019 my camp (Vagabonds)
    decided to gift a sauna to the community.
    This is a quick and dirty walkthrough for making the LED thermometer
    as seen on the picture.
    The hotter it gets the more LEDs
    are lit, and redder they become.
    In the picture the temperature  is 60°C and the range is 0-80°C
    </p>

<h2> Equipment </h2>
<table style="width:100%">
  <tr>
    <th>What </th>
    <th>Which </th>
    <th>Link </th>
  </tr>
  <tr>
    <td>Power Supply </td>
    <td>PHEVOS 5v 12A Dc Universal Switching Power Supply for Raspberry PI Models,CCTV </td>
    <td> tbd </td>
  </tr>

  <tr>
    <td>Micro Controller </td>
    <td> Rpi 3 </td>
    <td> tbd </td>
  </tr>

  <tr>
    <td>Thermometer </td>
    <td> Vktech 2M Waterproof Digital Temperature Temp Sensor Probe DS18b20 </td>
    <td> tbd </td>
  </tr>

  <tr>
    <td>Copper Wire</td>
    <td>  UL1015 Commercial Copper Wire, Bright, Red, 22 AWG, 0.0253" Diameter, 100' Length (Pack of 1) (As the leds draws about 6AMP its nice/wise to have a bit more then the standard RPI jumpers) </td>
    <td> tbd </td>
  </tr>

  </table>


<h2> Wiring </h2>
<div style="width:100%;background-color: white">
    <img width="100%" src="https://raw.githubusercontent.com/askasp/AB-LedThermostat/master/Wiring.png" />
    </div>


<h2> Code </h2>
<p> The code is rather strighforward as it builds upon  <a href="https://learn.adafruit.com/neopixels-on-raspberry-pi/python-usage">  NeoPixel  </a>
and <a href="https://github.com/timofurrer/w1thermsensor"> w1Thermsensor </a>.
Only small modifications were needed from the NeoPixel examples.

Note the MAX and MIN constants as they decide the temperature range for the termometer
</p>

<pre> <code>
#!/usr/bin/env python3

import time
from rpi_ws281x import *
import argparse
import math
from w1thermsensor import W1ThermSensor
sensor = W1ThermSensor()

MAX_TEMP = 100
MIN_TEMP = 0


# LED strip configuration:
LED_COUNT      = 300      # Number of LED pixels.
MAX_COLOR_LED = 200
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

def getColor(lednr):
    red = math.floor(lednr/MAX_COLOR_LED * 255)
    print("red is",red)
    print("lednr is",lednr)
    if red > 255:
        red = 255

    blue = 255-red

    if blue > red:
        green = 255 - blue
    else:
        green = 255 -red
        red = 255
        blue = 0
   
    return Color(red,green,blue)
    
def SetColor(temp):
    nr_of_leds =math.floor(LED_COUNT*temp/MAX_TEMP)
    print("nr of leds is",nr_of_leds)
    for i in range(0,LED_COUNT):
        if i < nr_of_leds:
            color = getColor(i)
        else:
            color = Color(0,0,0)
        strip.setPixelColor(i,color)
        strip.show()


# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:
        while True:
            SetColor(math.floor(sensor.get_temperature()))
            time.sleep(15)
          

    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip, Color(0,0,0), 10)


</code> </pre>





    """
  end
end
