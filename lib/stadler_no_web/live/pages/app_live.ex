defmodule StadlerNoWeb.AppLive do
  use StadlerNoWeb, :live_view

  @aksels_traits [
  "Marine Man", "Demonstrative Developer ", "Norwegian Native","Programming Person", "Vipps Vorker",
  "Ski Skier", "Rapid Runner", "Elixir Entusiast", "Golang Guy", "C/C++ Craftsman",
  "Burn Barbarian ", "Oslo Occupant", "Speedo Supporter", "Kindle Kindler", "Alliteration Assasinator", "Dota-2 Dabbler",
  "NixOS Nihilist", "Linux Learned ", "Narsisistic Nerd", "Fantasy Fanatic ", "Coffee Crafter",
  "Globe Goer", "Standby Soldier", "Tanzania Tourist " ]


  @impl true
  def mount(_params, _session, socket) do
      Process.send_after(self(), "new_trait", 1000)
    {:ok,
     assign(socket,
       show_menu: false,
       page: nil,
       query: "",
       trait: Enum.random(@aksels_traits),
       results: %{},
       front: {"me", "About me"},
       pages: [
         {StadlerNoWeb.LedThermometer.route(), StadlerNoWeb.LedThermometer.title()},
         {StadlerNoWeb.NixopsPhoenixPart1.route(), StadlerNoWeb.NixopsPhoenixPart1.title()},
         {StadlerNoWeb.Projects.route(), StadlerNoWeb.Projects.title()}
       ]
     )}
  end


  @impl true
  def handle_params(params, _uri, socket) do
    page = Map.get(params, "page")
    show_menu = false
    {:noreply, assign(socket, query: "", page: page, show_menu: show_menu)}
  end

  @impl true
  def handle_event("filter", %{"q" => query}, socket) do
    case query do
      "" ->
        {:noreply, socket}

      _ ->
        [front | new_page] =
          Enum.sort([socket.assigns.front | socket.assigns.pages], fn {_, t1}, {_, t2} ->
            distance(query, t1) > distance(query, t2)
          end)

        {:noreply, assign(socket, show_menu: true, query: query, front: front, pages: new_page)}
    end
  end

  def handle_event("key", %{"key" => "Escape"}, socket),
    do: {:noreply, assign(socket, show_menu: false)}

  def handle_event("key", _key, socket), do: {:noreply, socket}

  @impl true
  def handle_event("input-pressed", _a, socket), do: {:noreply, assign(socket, show_menu: true)}

  def handle_event("navigate", %{"page" => page}, socket),
    do: {:noreply, push_patch(socket, to: "/#{page}")}

  @impl true
  def handle_event("input-released", _a, socket) do
    {:noreply, assign(socket, show_menu: false)}
  end

  def handle_event("navigate-to-highlighted", _a, socket) do
    {page, _} = socket.assigns.front
    {:noreply, push_patch(socket, to: "/#{page}")}
  end


  @impl true
  def handle_info("new_trait", socket) do
      Process.send_after(self(), "new_trait", 1000)
      {:noreply, assign(socket, trait: Enum.random(@aksels_traits))}
      end

  def distance(a, b), do: FuzzyCompare.ChunkSet.standard_similarity(a, b)


end
