
defmodule StadlerNoWeb.Projects do
  use StadlerNoWeb, :live_component

  def title(), do: "Projects"
  def route(), do: "projects"

  def render(assigns) do
    ~L"""
    <h1> </h1>
    <h2> Projects </h2>
    <hr class="solid">

    <r-grid style="align-items: center;" columns=8 columns-s=1>
    <r-cell span=3>
    <img width="100%" src="https://raw.githubusercontent.com/askasp/AB-LedThermostat/master/saunandtermo.png"/>
    </r-cell>
    <r-cell span=5>
    <h3> <a href="/led"> LED thermometer for Africa Burn </a> </h3>
    <p> In Africa burn 2019 we gifted a sauna to the community. We installed a 3m tall LED thermometer so bypassers could see
    the current sauna temperature.
    Here is a walkthrough of the code and how I wired it all up </p>
    </r-cell>
    </r-grid>

    <r-grid style="align-items: center;" columns=8 columns-s=1>
    <r-cell span=3>
    <img width="100%" src="https://beyermatthias.de/2015-11-15-nixos_wallpaper.png"/>
    </r-cell>
    <r-cell span=5>
    <h3> <a href="/nixops"> Phoenix liveview & nixops (this webpage) </a> </h3>
    <p>
    Phoenix liveview is the new go-to framework for building SPAs in much the same
    way that 2021 is the year of the desktop (fingers crossed).
    Its killer feature  is server-side-rendered dynamic webpages (it's as great as
    it sounds). If you think the dynamic text on the top of this page is pure JS - think again.

    Nixops is for people that are too cool for kubernetes.

    </p>
    </r-cell>

    </r-grid>
    """
  end
end
