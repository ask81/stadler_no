defmodule StadlerNoWeb.AboutMe do
  use StadlerNoWeb, :live_component

  def title(), do: "About me"
  def route(), do: "/about_me"




  def render(assigns) do
    ~L"""
    <section>

    <div class="centered">
    <h1> Aksel Stadler </h1>
    <subtitle> Robotics Engineer & Programmer </subtitle>
    </div>




    </section>
    """
  end
end
