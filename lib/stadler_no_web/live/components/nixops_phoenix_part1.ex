defmodule StadlerNoWeb.NixopsPhoenixPart1 do
    use StadlerNoWeb, :live_component

    def title(), do: "Phoenix Liveview & Nixops on Digital Ocean"
    def route(), do: "nixops"

    def render(assigns) do
        ~L"""
<h1> <%= title() %> </h1>
<hr class="solid">

<p>
Phoenix liveview enables server-side-rendered <b> ''single-page''</b> web pages.
It does this by the power of a brand-new technology (not really) called websockets.

</br> 
</br> 
Liveview sets up a websocket connection between the client and the server.
Commands goes from the client, and an updated html is sent back.
Morphdom then seemlessly updates the view.

</br>

</br>
Why isn't this implemented in other backend languages you might wonder.
Well, mostly because websockets are statefull, and many languages are not
optimised for that.
</br>

</br>
In Elixir/Erlang state is trivial. This is because the latter is an (unintentional)
implementation of the actor model. Each websocket connection is an totally isolated
process (actor) that can only communicate with other actors through message passing.
I.e. Share by communicating, don't communicate by sharing (i'm looking at 
you  C++)

<p>
NixOS is a declerative linux distribution built around the nix package manager.
It enables, and encourages, users to declare their system in a version controlled
config file. This makes rebuilding and duplication trivial. This is possible
as nix packages are 100% declerative. Each package is built by pure functions,
guaranteeing that it is identical each time it is built.

</br>
</br>
Nixops allows you to define a NixOS system, build it locally, and ship it
to one or more remote NixOS machines.
</p>


<h2> Why not just use Kubernetes? </h2>
<p>
It seems that kubernetes is about to become the de-facto standard for
hosting. At-least for companies that are worried about cloud-provider lock-in
(which they should be). Also, with microk8s it's cheap to spin up a cluster
for hobby projects as well.

The reason I, currently, choose to use nixops for my projects are.


<ul class="bullet-list">
<li > No need for a container registry </li>
<li> Nix-shell on the default.nix makes a shell with all dependencies to
build and run the project </li>
<li> Use native Linux tools, like systemd, directly. </li>
<li> It handles both the software, and the infrastrucure, making it trivial to change
vendor</li>
<li> I'm the type of person that throws away the IKEA manual, and get frustrated when I can't fit
the pieces </li>

</ul>


<h2> Why not both? </h2>
<a href="https://nixos.wiki/wiki/Kubernetes"> Be my guest! </a>

<h2> Making the nix package derivation </h2>

<p> First create a new phoenix project with</p>
<code>
mix phx.new nixops_live --live --no-ecto
</code>
<p> You can easily test it by running </p>
<code>
mix phx.server
</code>
and go to localhost:4000. 


<p>
Now, we create a derivation for how nix should build this project.
Create a new file named default.nix
and fill in
</p>


<pre><code class="plaintext">

with import <nixpkgs> {};

let
stadler_no = {}:
 stdenv.mkDerivation rec {
  name = "stadler_no";
  src = ./.;
  buildInputs = [elixir git nodejs];

  #Certs are only needed if you pull repos directly from github

  buildPhase = ''
  mkdir -p $PWD/.hex
  export HOME=$PWD/.hex
  export GIT_SSL_CAINFO=/etc/ssl/certs/ca-certificates.crt
  export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
  mix local.rebar --force
  mix local.hex --force
  mix deps.get
  MIX_ENV=prod mix compile
  npm install --prefix ./assets
  npm run deploy --prefix ./assets
  mix phx.digest
  MIX_ENV=prod mix release

  '';
  installPhase = ''
    mkdir -p $out
    cp -rf _build/prod/rel/stadler_no/* $out/

  '';
};
in
stadler_no

</code></pre>

<p>
There are multiple things going on here. For a more thorough explanation of nix packages I recommend this
<a href="https://christine.website/blog/nixos-desktop-flow-2020-04-25"> this </a> blogpost by Christine Dodrill.
What you need to pay attention to is
<ol class="bullet-list">
<li> buildInputs -- (The requirements to build) </li>
<li> buildPhase --  (A bash recipe for building phoenix releases) </li>
<li> install phase -- (Copying the binary to an artifact directory) </li>
</ol>

<p> You can test this by running </p>
<pre> <code> nix-build default.ex --option sandbox false</code> </pre>
<p> The latter is needed as we fetch deps from the internet. If successfull you should
see a result folder in your directory. </p>


<p> Note: the default.nix can also be used to set up a development environment. Simply run </p>
<pre> <code> nix-shell defualt.nix </code> </pre>
<p> and you will drop into a shell with all the needed dependencies installed. This is
supernice when multiple devs are working on the same project </p>


<h2> Making the nix service </h2>
<p> nix-build only compile the code, is does not run it.  Next step is to set up a service for that. A
systemd service to be exact. Create the file service.nix and fill it with </p>

<pre><code>
{config, lib, pkgs, ...}:
    let

        #Build the derivation described in default.nix
    	stadler_no_build = pkgs.callPackage ./default.nix {};
    	cfg = config.services.stadler_no;

in {

    #Set configurable options
    options.services.stadler_no.enable = lib.mkEnableOption "stadler_no";
    options.services.stadler_no.port = lib.mkOption {
        type = lib.types.int;
        default = 4000;
    };

    config = lib.mkIf cfg.enable {

    networking.firewall.allowedTCPPorts = [ cfg.port ];


    #Define the service
    systemd.services.stadler_no = {
        description = "My home page stadler no";
        environment = {
            HOME="/root";
            PORT="${toString cfg.port}";
            RELEASE_TMP="/tmp";
            #Secrets will be injected at a later time
            ADMIN_PWD=''$(cat /run/keys/admin_pwd)'';
            Stadler_NO_SKB=''$(cat /run/keys/stadler_no_skb)'';
            Stadler_NO_LV_SS=''$(cat /run/keys/stadler_no_lv_ss)'';
            };

     after = [ "network-online.target" ];
     wantedBy = [ "network-online.target" ];


    serviceConfig = {
        DynamicUser = "true";
        PrivateDevices = "true";
        ProtectKernelTunables = "true";
        ProtectKernelModules = "true";
        ProtectControlGroups = "true";
        RestrictAddressFamilies = "AF_INET AF_INET6";
        LockPersonality = "true";
        RestrictRealtime = "true";
        SystemCallFilter = "@system-service @network-io @signal";
        SystemCallErrorNumber = "EPERM";

	#This command is run when the systemd service starts
        ExecStart = "${stadler_no_build}/bin/stadler_no start";
        Restart = "always";
        RestartSec = "5";
    };
    };
};
}

</code></pre>

<p> Commit the code and push it to GitLab </p>


<h2> Nixops </h2>
<p> It's time to set up the server. You can naturally use the
same repo as before, but I like creating a new one as I have many projects
in the same deployment. </p>

<p> Create a new directory named nixops_do, and generate a ssh keypair here.
Be sure to place the keys in the nixops_do, and not overwrite the one in ~/.ssh.
Keep the passphrase empty </p>

<p> After this, go to digitalocean.com, create an ubuntu droplet, and add
the newly generated key under authorized hosts.
<a href="https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-existing-droplet/"> Tutorial </a>

<p> Make three empty files, nixops_do.nix, nixops_do_hw.nix, and nixops_do_secrets.nix


The first will describe what is running on the machine, the second describes the machine,
and the latter injects secrets as env variables </p>

<h3> nixops_do.nix </h3>

<pre> <code>

## Fetch service from gitlab
stadler_no = builtins.fetchGit {
      url = "https://gitlab.com/akselsk/stadler_no";
      ref = "master";
      rev = "82b0e190d10c70c1878d1d61e3b7e1618f14d822";
};

in
    let backend= { config, pkgs, ... }: {
  	environment.systemPackages = with pkgs; [cacert git vim ];

	# Add public key to enable updates
  	users.users.root = {
    	openssh.authorizedKeys.keys = ["ssh-ed25519 **pubkey** aksel@stadler.no"];
  	};

    services.openssh.enable = true;
  	#Import the service file made previously
  	imports = [
      	"${stadler_no.outPath}/service.nix"

  	];
        networking.firewall.allowedTCPPorts = [ 22 4343 80 443 ];
        # Enable the service
  	services.stadler_no.enable = true;
  	services.stadler_no.port= 4002;
  	services.openssh.permitRootLogin = "yes";

 	#Allow the erlang vm to write error logs to disk
	systemd.tmpfiles.rules = [
  	"d /root/tmp 0755 root root"
	];

	#Start nginx procy pass
        services.nginx = {
            enable = true;
            recommendedProxySettings = true;
            recommendedTlsSettings = true;

            virtualHosts."stadler.no" =  {
              enableACME = true;
              forceSSL = true;
              locations."/" = {
                proxyPass = "http://127.0.0.1:4002";
                proxyWebsockets = true; # needed if you need to use WebSocket
              };
            };

        };

        security.acme.acceptTerms = true;
	security.acme.certs = {
  	"stadler.no".email = "aksel@stadler.no";
	};

    	};


in

{

  network.description = "Test server";
  #The server name, must have a corresponding hardware name
  asknixops1= backend;

  #If desireably to deploy to multiple machines
  #asknixops2= backend
}

</code> </pre>


<h3> nixops_do_hw </h3>
<pre> <code>

  # Hardware config
 let machine = { config, pkgs, ... }: {
    deployment.targetEnv = "digitalOcean";
    deployment.digitalOcean.enableIpv6 = true;
    deployment.digitalOcean.region = "ams3";
    deployment.digitalOcean.size = "s-1vcpu-1gb";
    deployment.digitalOcean.vpc_uuid= "default-ams3";
   # deployment.digitalOcean.authToken = "Doesn't seem to work";
  };
in
{
  resources.sshKeyPairs.ssh-key = {
    publicKey = builtins.readFile ./tstKey.pub;
    privateKey = builtins.readFile ./tstKey;
  };

  asknixops1= machine; #Much mach a corresponding server

  #asknixops2 = machine;

}

</code></pre>


<h3> nixops_do_secrets </h3>
<pre> <code>
  asknixops1=
    { config, pkgs, ... }:
    {
      deployment.keys.admin_pwd.text = "***";
      deployment.keys.stadler_no_skb = "**";
      deployment.keys.stadler_no_lv_ss = "**";
    };
}
</pre> </code>


<p> Create the deployment by running </p>
<pre> <code> nixops create ./nixops_do.nix ./nixops_do_hw.nix ./nixops_do_secrets.nix -d nixops_do </code> </pre>
<p> and ship it with </p>

<pre> <code> nixops deploy -d nixops_do </code> </pre>

For debugging you can ssh into the machine with
<pre> <code> nixops ssh *deployname* *machine_name*  </code> </pre>
<p> To print debug logs run the following </p>
<pre> <code> systemctl status *service_name* or journalctl -u *service_name* </pre> </code>

"""
    end

end

