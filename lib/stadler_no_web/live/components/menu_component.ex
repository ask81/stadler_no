 
defmodule AboutMePage do
  use StadlerNoWeb, :live_component

  def render(assigns) do
    ~L"""
    <h3> Aksel Stadler  - MsC Cybernetics and working as a software developer</h3>


    <h5> Skills</h5>
    <ul>
    <li> Algorithms and data structures </li>
    <li>  Phoenix w. liveview, Elixir, C/C++, Go, F#, React, and others</li>
    <li>  RPI, Arduino, buildroot, (embedded systems) /li>
    <li> Algorithms and data structures </li>
    <li> Docker, K8s, Nginx </li>

    </ul>

    <h5> Skills in the making  </h5>
    <ul>
    <li> Elixir Nerves  </li>
    <li> AWS, GCE, Azure, Nixops </li>
    <li> GRPC </li>
    <li> CQRS, ES, DDD </li>
    </ul>



"""
  end

  end
