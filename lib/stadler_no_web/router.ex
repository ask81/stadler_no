defmodule StadlerNoWeb.Router do
  import Phoenix.LiveDashboard.Router
  import Plug.BasicAuth

  use StadlerNoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {StadlerNoWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end


  scope "/" do
    pipe_through [:browser, :auth]
    live_dashboard "/dashboard", metrics: StadlerNoWeb.Telemetry
  end

  scope "/", StadlerNoWeb do
    pipe_through :browser

    live "/", AppLive, :index
    live "/:page", AppLive, :index
  end


defp auth(conn, _opts) do
  with {"admin", pass} <- Plug.BasicAuth.parse_basic_auth(conn),
       true <- Plug.Crypto.secure_compare(pass, Application.get_env(:stadler_no, :password)) do
       assign(conn, :current_user, "admin")
  else
    _ -> conn |> Plug.BasicAuth.request_basic_auth() |> halt()
  end

end


end
