#/bin/bash
version=$(mix edeliver version production --plain | grep -Po '(?<=response: )[^;]+' | tr -d '\n' | tr -d \'\" | tr -d '[:space:]')

if [ -z "$version" ]; then
      echo "No code is running, making a new release"
      mix edeliver build release --plain
      mix edeliver deploy release to production --plain
      mix edeliver restart production --plain
else
      echo "Production code exists, uppgrading"
      mix edeliver build upgrade --with="$version" --plain
      mix edeliver deploy upgrade to production --verbose --plain
fi





